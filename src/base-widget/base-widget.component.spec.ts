import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseWidgetStreamingComponent } from './base-widget.component';

describe('BaseWidgetStreamingComponent', () => {
  let component: BaseWidgetStreamingComponent;
  let fixture: ComponentFixture<BaseWidgetStreamingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseWidgetStreamingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseWidgetStreamingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
