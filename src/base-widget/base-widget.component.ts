import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetStreamingComponent implements OnInit {
  public dataLoader: Array<any> = [
	{
		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
		"image": "../../assets/default.jpg",
		"timestamp": "15 minutes ago",
		"media_display": "Agence France-Presse",
		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
	},
  {
		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
		"image": "../../assets/default.jpg",
		"timestamp": "15 minutes ago",
		"media_display": "Agence France-Presse",
		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
	},
  {
		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
		"image": "../../assets/default.jpg",
		"timestamp": "15 minutes ago",
		"media_display": "Agence France-Presse",
		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
	}
];

  constructor() {console.log(this.dataLoader);}
  
  ngOnInit() { 
  }

}
