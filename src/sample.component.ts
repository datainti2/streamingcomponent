import { Component } from '@angular/core';

@Component({
  selector: 'sample-component',
  template: `<app-base-widget></app-base-widget>`
})
export class SampleComponent {

  constructor() {
  }

}
