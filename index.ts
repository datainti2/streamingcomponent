import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetStreamingComponent } from './src/base-widget/base-widget.component';
export * from './src/base-widget/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetStreamingComponent
  ],
  exports: [
  BaseWidgetStreamingComponent
  ]
})
export class StreamingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StreamingModule,
      providers: []
    };
  }
}
